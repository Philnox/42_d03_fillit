REF_FIGS "reference" - you can add a file with name "reference" in main folder. And figures in this file will be a valid figs. Exception figure with full line and more.
Don't use REF_FIGS "reference" with FIG_NO_CHANGE_REF 0

.#..
###.
.#..
.... ok

....
####
....
.... ok

....
####
#...
.... not ok
