https://trello.com/b/PfGPIKIl/42fillit

Norminette
{
	cd ~/d03_fillit_gitlab/prod/2push
	clear && find . -maxdepth 1 -type f | xargs norminette
	norminette
}

Leaks
{
	valgrind --leak-check=full ./fillit ../test/pdf
}


Makefile
{
	https://stackoverflow.com/questions/2746553/read-values-into-a-shell-variable-from-a-pipe

	find . -maxdepth 1 -name '*.c' | sed "s/\.\///" | sed "s/\.c//" | while read x ; do echo "$x.o:\n\t\$(CC) \$(CFLAGS) $x.c" ; done

	find . -maxdepth 1 -name '*.c' | sed "s/\.\///" | sed "s/\.c/\.o/" | tr '\n' ' '
}
